#!/bin/sh
NGINX_CONFIG_TEMPLATE=/template_nginx.conf
NGINX_CONFIG_FILE=/opt/nginx/conf/nginx.conf

echo "Building config..."
sed -e 's/~STREAM_SERVICE~/'"$STREAM_SERVICE"'/g' -e 's/~STREAM_SERVICE_KEY~/'"$STREAM_SERVICE_KEY"'/g' -e 's/~STREAM_PUBLISH_KEY~/'"$STREAM_PUBLISH_KEY"'/g' -e 's/~STREAM_VIEW_KEY~/'"$STREAM_VIEW_KEY"'/g' -e 's/~STREAM_MASTER_KEY~/'"$STREAM_MASTER_KEY"'/g' $NGINX_CONFIG_TEMPLATE > $NGINX_CONFIG_FILE

echo "Creating HLS dir..."
mkdir -p /usr/local/nginx/html/HLS/live
chmod 777 /usr/local/nginx/html/HLS/live

echo "Starting server..."
/opt/nginx/sbin/nginx -g "daemon off;"