const fs = require("fs-extra");
const path = require("path");
let jwt = require("jsonwebtoken");

module.exports = {
  environmentOK: () => {
    let fail =
      !process.env.STREAM_SERVICE ||
      !process.env.STREAM_SERVICE_KEY ||
      !process.env.STREAM_PUBLISH_KEY ||
      !process.env.JWT_SECRET ||
      !process.env.STREAM_VIEW_KEY;

    if (fail) module.exports.log("FATAL: Environment variables not set!");
    return !fail;
  },

  log: (message) => {
    if (
      process.env.DEBUG_MODE &&
      process.env.DEBUG_MODE.trim().toLowerCase() === "true"
    )
      console.log(message);
  },

  saveJSONToFile: (json, directory, file) => {
    if (typeof json === "undefined") {
      console.error(`Cannot save ${file} to package, data missing in post.`);
      return;
    }
    let outputFile = fs.createWriteStream(path.join(directory, file), {
      flags: "w",
    });
    outputFile.write(JSON.stringify(json), () => {
      outputFile.end();
    });
  },

  decodeToken: (reqOrToken, onSuccess, onFail) => {
    let token;
    if (reqOrToken.headers) {
      let bearerHeader = reqOrToken.headers["authorization"];
      if (typeof bearerHeader !== "undefined") {
        const bearer = bearerHeader.split(" ");
        token = bearer[1];
      }
    } else {
      token = reqOrToken;
    }
    jwt.verify(
      token,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err, decodedToken) => {
        if (err) {
          if (typeof onFail === "function") {
            onFail(err);
          } else {
            console.error(err);
          }
        } else {
          onSuccess({ encoded: token, decoded: decodedToken });
        }
      }
    );
  },
};
