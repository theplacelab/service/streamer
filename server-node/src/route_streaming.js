const axios = require("axios");
const xml2js = require("xml2js");
const crypto = require("crypto-js");
const xmlParser = new xml2js.Parser({ explicitArray: false });
var md5 = require("md5");

const hashString64 = (string) => {
  const hash = crypto.MD5(string);
  const base64Encoded = crypto.enc.Base64.stringify(hash)
    .replace(/=/g, "")
    .replace(/\+/g, "-")
    .replace(/\//g, "_");
  return base64Encoded;
};

const hashStringHex = (string) => {
  return crypto.MD5(string).toString(crypto.enc.Hex);
};

module.exports = {
  info: (req, res) => {
    axios
      .get(
        `https://${process.env.STREAM_SERVICE}/s/${hashStringHex(
          `stats${process.env.STREAM_SERVICE_KEY}`
        )}/stats`
      )
      .then((response) => {
        if (response.status === 200) {
          console.log(response);
          xmlParser.parseString(response.data, function (err, result) {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.write(
              JSON.stringify({
                uptime: result.rtmp.uptime,
                last_checked: Date.now(),
              })
            );
            res.end();
          });
        } else {
          res.sendStatus(500);
        }
      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(403);
      });
  },
  infoSecure: (req, res) => {
    axios
      .get(
        `https://${process.env.STREAM_SERVICE}/s/${hashStringHex(
          `stats${process.env.STREAM_SERVICE_KEY}`
        )}/stats`
      )
      .then((response) => {
        if (response.status === 200) {
          console.log(response);
          xmlParser.parseString(response.data, function (err, result) {
            res.writeHead(200, { "Content-Type": "text/html" });
            res.write(
              JSON.stringify({ ...result.rtmp, last_checked: Date.now() })
            );
            res.end();
          });
        } else {
          res.sendStatus(500);
        }
      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(403);
      });
  },

  setup: (req, res) => {
    const broadcastExpiration =
      Math.round(Date.now() / 1000) +
      process.env.STREAM_PUBLISH_EXPIRATION_TIME_MINUTES * 60;

    const viewExpiration =
      Math.round(Date.now() / 1000) +
      process.env.STREAM_VIEW_EXPIRATION_TIME_MINUTES * 60;

    const broadcast_SERVICE = `rtmp://${process.env.STREAM_SERVICE}/ingress`;

    const broadcast_key = `${req.params.id}?md5=${hashString64(
      `${process.env.STREAM_PUBLISH_KEY}ingress/${req.params.id}${broadcastExpiration}`,
      broadcastExpiration
    )}&expires=${broadcastExpiration}`;

    const broadcast_rtmp = `${broadcast_SERVICE}/${broadcast_key}`;

    const view_rtmp = `rtmp://${process.env.STREAM_SERVICE}/live/${
      req.params.id
    }?md5=${hashString64(
      `${process.env.STREAM_VIEW_KEY}live/${req.params.id}${viewExpiration}`,
      viewExpiration
    )}&expires=${viewExpiration}`;

    const view_hls = `https://${process.env.STREAM_SERVICE}/video/${req.params.id}.m3u8`;

    var generatedLinks = {
      broadcast_SERVICE,
      broadcast_key,
      broadcast_rtmp,
      view_rtmp,
      view_hls,
    };
    res.writeHead(200, { "Content-Type": "text/html" });
    res.write(JSON.stringify(generatedLinks));
    res.end();
  },

  status: (req, res) => {
    axios
      .get(
        `https://${process.env.STREAM_SERVICE}/s/${hashStringHex(
          `stats${process.env.STREAM_SERVICE_KEY}`
        )}/stats`
      )
      .then((response) => {
        if (response.status === 200) {
          xmlParser.parseString(response.data, function (err, result) {
            res.writeHead(200, { "Content-Type": "text/html" });
            var streams = result.rtmp.server.application.find(
              (app) => app.name === "ingress"
            ).stream;
            if (streams && !Array.isArray(streams)) streams = [streams];
            res.write(
              JSON.stringify({
                is_live: streams
                  ? streams.find((stream) => stream.name === req.params.id)
                    ? true
                    : false
                  : false,
                last_checked: Date.now(),
              })
            );
            res.end();
          });
        } else {
          res.sendStatus(500);
        }
      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(403);
      });
  },

  statusSecure: (req, res) => {
    axios
      .get(
        `https://${process.env.STREAM_SERVICE}/s/${hashStringHex(
          `stats${process.env.STREAM_SERVICE_KEY}`
        )}/stats`
      )
      .then((response) => {
        if (response.status === 200) {
          xmlParser.parseString(response.data, function (err, result) {
            res.writeHead(200, { "Content-Type": "text/html" });
            var streams = result.rtmp.server.application.find(
              (app) => app.name === "ingress"
            ).stream;
            if (streams && !Array.isArray(streams)) streams = [streams];
            res.write(
              JSON.stringify({
                ...(streams
                  ? streams.find((stream) => stream.name === req.params.id)
                  : ""),
                is_live: streams
                  ? streams.find((stream) => stream.name === req.params.id)
                    ? true
                    : false
                  : false,
                last_checked: Date.now(),
              })
            );
            res.end();
          });
        } else {
          res.sendStatus(500);
        }
      })
      .catch((err) => {
        console.log(err);
        res.sendStatus(403);
      });
  },
};
