"use strict";
require("dotenv").config({ path: require("find-config")(".env") });
const middleware = require("./src/middleware.js");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const util = require("./src/util.js");
const route_streaming = require("./src/route_streaming.js");
const app = express();
const fs = require("fs");
const https = require("https");

if (!util.environmentOK()) {
  console.error("FATAL: Environment not configured properly");
  app.all("*", (req, res) => {
    res.sendStatus(500);
  });
} else {
  console.log(`\n--------------------------------------`);
  console.log(`NGIX Streamer Support Server`);
  console.log(process.env.STREAM_SERVICE);
  console.log(`Port: ${process.env.PORT || 8888}`);
  console.log(`--------------------------------------`);

  app.use(cors());
  app.use(bodyParser.json());
  app.engine("html", require("ejs").renderFile);

  try {
    let certificate = fs.readFileSync("/cert/fullchain.pem");
    let privateKey = fs.readFileSync("/cert/privkey.pem");
    https
      .createServer(
        {
          key: privateKey,
          cert: certificate,
        },
        app
      )
      .listen(process.env.PORT || 8888);
    console.log("SSL: ENABLED");
  } catch {
    console.log("SSL: DISABLED - CERTIFICATE NOT FOUND");
    app.listen(process.env.PORT || 8888);
  }
  //Streaming
  app.get("/streaming/info", (req, res) => {
    middleware.verifyWithFallback(
      req,
      res,
      () => route_streaming.infoSecure(req, res),
      () => route_streaming.infoSecure(req, res)
    );
  });

  app.get("/streaming/setup/:id", middleware.verifyToken, (req, res) => {
    route_streaming.setup(req, res);
  });

  app.get("/streaming/status/:id", (req, res) => {
    middleware.verifyWithFallback(
      req,
      res,
      () => route_streaming.statusSecure(req, res),
      () => route_streaming.status(req, res)
    );
  });

  // Fallthrough
  app.use(function (req, res) {
    util.log(`REJECTING: ${req.method}:${req.originalUrl}`);
    res.sendStatus(404);
  });
}
