# NGINX Streamer

Creates a streaming server using just NGINX and a node server to manage this. The `.gitlab-ci.yml` file builds and packages these for Docker/K8, sample Docker configs included.

# server-nginx

Actual streaming server, based on NGINX. Supports multiple ingress, secure links and timeouts. Supports RTMP and HLS. This is more complicated than plain streaming (and possibly not necessary). If you don't need all of this, consider using [JasonRivers](https://github.com/JasonRivers/Docker-nginx-rtmp) instead, which is more straightforward.

# server-node

Node server to generate the proper signed links. Expects token-signed GET requests. Expects the streaming server to be running:

`/info`
`/streaming/info`
`/streaming/setup/:id`
`/streaming/status/:id`

In order to make use of the server you need to generate "magic links," this is handled by the API provided by the node app in the supportServer directory.

- Be sure to set up the `.env` properly
- Have the streamer running somewhere, this repo will create a package at: `registry.gitlab.com/theplacelab/image/streamer` and there is a sample `docker-compose.yml` in the example dir.
